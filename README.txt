Theme permissions
--------------------

Goals:
=> Create permissions based on themes
=> Provide simple permissions for other modules to restrict access to any drupal themes.

Installation:
-------------
drush en theme_permission
OR
Go to: /admin/modules/ and enable theme pemrissions.

Configuration:
---------------
Go to: /admin/config/content/theme_permissions and choose your vocabularies.

Example usage:
--------------

if(!user_access('use '.$theme_machine_name.' category')) {
  drupal_set_message(t('AHAH, you have no permission to user this theme! :P'));
  return FALSE;
}
